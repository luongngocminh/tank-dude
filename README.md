# Platformer vertical shooting game using SDL2 and SDL_image
## Dependencies
1. Visual Studio
2. SDL2 Library
3. SDL_image Library

## Setup the environment
1. Move the vs_dev_lib folder to the root of C:/
2. Setup the project environment:
   1. Go to Project tab -> tank-dude Properties -> VC++ Directories
   2. Change the Include Directories by clicking on the this button, then click Edit
    ![Preview1](./img1.PNG)
   1. Click the New Line button -> click the new line -> click the "..." button and find to C:/vs_dev_lib/SDL2-2.0.10/include and press OK
    ![Preview2](./img2.PNG)
   2. Do the same for the Library Directories but with C:/vs_dev_lib/SDL2-2.0.10/lib/x86
   3. In the Property Pages, go to Linker -> Input and add "SDL2.lib;SDL2main.lib;SDL2_image.lib;SDL2_ttf.lib" (without the ") to Additional Dependencies
3. Set the platform the Win32 and the Debugger to x86