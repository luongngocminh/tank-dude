﻿#include <SDL.h>
#include "structs.h"
#include "defs.h"
#include "init.h"
#include "input.h"
#include "draw.h"
#include "player.h"
#include "bullet.h"
#include "target.h"
#include <time.h>
#include <stdlib.h>
//#include <stdio.h>
int main(int argc, char* argv[])
{
	App app;
	app.score = 0;

	Init init(&app);
	init.initSDL();
	Input input(&app);
	Draw draw(&app);
	int playerH = PLAYER_HEIGHT;
	int playerW = PLAYER_WIDTH;
	int playerX = SCREEN_WIDTH / 2 - playerW / 2;
	int playerY = SCREEN_HEIGHT - playerH;
	float speed = PLAYER_SPEED;

	Target targets[150];
	Target targetGenerator;

	Player player(&app, playerX, playerY, playerW, playerH, speed);
	Bullet bullet(&app, player.x, playerY, BULLET_SPEED, 0);
	srand((unsigned)time(NULL));
	targetGenerator.generateTargets(&targets[0]);

	unsigned int x_milliseconds, target_milliseconds, elapsedDecayTime, x_seconds = 0, x_minutes = 0
		, time_left = 0, decayTimeLeft = 0, count_down_time_in_secs = TIME_PLAY;
	clock_t x_startTime, x_countTime, target_startDecayTime;
	x_startTime = clock();
	target_startDecayTime = clock();
	time_left = count_down_time_in_secs - x_seconds;
	while (time_left > 0)
	{
		draw.prepareScene();
		input.doNewFrame();
		if (targets[0].appear == 0) {
			targetGenerator.generateTargets(&targets[0]);
			target_startDecayTime = clock();
		}

		x_countTime = clock();
		x_milliseconds = x_countTime - x_startTime;
		target_milliseconds = x_countTime - target_startDecayTime;
		x_seconds = (x_milliseconds / (CLOCKS_PER_SEC)) - (x_minutes * 60);
		elapsedDecayTime = (target_milliseconds / (CLOCKS_PER_SEC)-(x_minutes * 60));

		time_left = count_down_time_in_secs - x_seconds; 
		//printf("Time left: %d\n", time_left);
		decayTimeLeft = DECAY_TIME - elapsedDecayTime;
		//printf("Decay Time left: %d\n", decayTimeLeft);

		if (decayTimeLeft <= 0) {
			targets[0].appear = 0;
		}
		draw.drawTimerBar(time_left, app.renderer);
		draw.drawTargets(&targets[0], app.renderer);
		draw.drawPlayer(player, app.renderer);
		draw.drawBullet(bullet, app.renderer);
		draw.drawScore(app, app.renderer, 0);
		SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 255);

		input.doInput();
		player.update();
		bullet.updateBullet(player.x);
		bullet.checkCollision(&app, &targets[0]);
		draw.presentScene();
		SDL_Delay(16);
	}
	draw.prepareScene();
	draw.drawScore(app, app.renderer,1);
	draw.presentScene();
	SDL_Delay(2000);
	return 0;
}