﻿#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define PLAYER_HEIGHT 30
#define PLAYER_WIDTH 65
#define PLAYER_SPEED 4
#define BULLET_SPEED 40
#define BULLET_HEIGHT 20
#define BULLET_WIDTH 10
#define TARGET_NUMBER 1
#define TARGET_DIMENSION 20 
#define TIME_PLAY 120 // Seconds Thời gian chơi
#define DECAY_TIME 3 // Seconds Thời gian target tồn tại