#ifndef TARGET_H
#define TARGET_H

class Target {
public:
	Target();
	~Target();
	int x;
	int y;
	int type;
	bool appear;
	void generateTargets(Target* target);
};

#endif // !TARGET_H
