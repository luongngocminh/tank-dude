#ifndef INIT_H
#define INIT_H
#include "structs.h"

class Init
{
public:
	Init(App* app);
	~Init();

	void initSDL();
	void clearApp();
private:
	App* app;
};
#endif // !INIT_H


