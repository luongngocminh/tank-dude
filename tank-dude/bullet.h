#ifndef BULLET_H
#define BULLET_H
#include "structs.h"
#include "player.h"
#include "target.h"
class Bullet {
public:
	Bullet(App* app, int x, int y, float bulletSpeed, bool appear);
	~Bullet();
	int x;
	int y;
	float bulletSpeed;
	bool appear;
	void updateBullet(int playerX);
	void checkCollision(App* app, Target* target);
private:
	App* app;
};
#endif // !BULLET_H