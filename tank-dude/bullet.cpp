#include <SDL.h>
#include "bullet.h"
#include <stdio.h>
#include "defs.h"
#include "player.h"
#include "target.h"
#include "draw.h"
Bullet::Bullet(App* app, int x, int y, float bulletSpeed, bool appear) {
	this->app = app;
	this->x = x;
	this->y = y;
	this->appear = 0;
	this->bulletSpeed = bulletSpeed;
}
Bullet::~Bullet() {

}
void Bullet::updateBullet(int playerX) {
	if (!appear) { this->x = playerX+(PLAYER_WIDTH-BULLET_WIDTH)/2; }
	if (appear) { y -= bulletSpeed; }
	if (app->space)
	{
		appear = 1;	
	}
	if (y < 0) 
	{ 
		appear = 0;
		y = SCREEN_HEIGHT;
	}
}
void Bullet::checkCollision(App* app, Target* target) {
	int range;
	if (target->type == 1) range = 20;
	else if (target->type == 2) range = 22;
	else range = 21;
	if ((target->x + range >= this->x) && (target->x - range <= this->x)
		&& (target->y + range >= this->y) && (target->y-BULLET_SPEED <= this->y)) {
		target->appear = 0;
		this->appear = 0;
		this->y = SCREEN_HEIGHT;
		app->score += target->type;
	}
}