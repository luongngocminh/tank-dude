#ifndef DRAW_H
#define DRAW_H
#include "structs.h"
#include "player.h"
#include "bullet.h"
#include "target.h"

class Draw
{
public:
	Draw(App* app);
	~Draw();

	void prepareScene();
	void presentScene();
	void drawPlayer(Player player, SDL_Renderer* renderer);
	void drawBullet(Bullet bullet, SDL_Renderer* renderer);
	void drawTargets(Target* target, SDL_Renderer* renderer);
	void drawScore(App app, SDL_Renderer* renderer, bool checkWin);
	void drawTimerBar(int time_left, SDL_Renderer* renderer);
private:
	App* app;
};
#endif // !DRAW_H


