#include "target.h"
#include "defs.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
Target::Target() {

}

Target::~Target() {

}

void Target::generateTargets(Target* target) {
	int xRange = ( SCREEN_WIDTH - 50 ) + 1;
	int yRange = ( SCREEN_HEIGHT - 120 ) + 1;
	int randX = 10 + rand() % xRange;
	int randY = 40 + rand() % yRange;
	int randType = 1 + rand() % 3;
	target->x = randX;
	target->y = randY;
	target->type = randType;
	target->appear = 1;
}