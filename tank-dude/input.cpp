#include <SDL.h>
#include "input.h"
#include <stdio.h>
#include "bullet.h"
#include "player.h"
#include "defs.h"
Input::Input(App* app) {
	this->app = app;
}

Input::~Input() {

}

void Input::doNewFrame() {
	//app->left = 0;
	//app->right = 0;
	app->space = 0;
}

void Input::doInput() {
	SDL_Event event;
	
	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_QUIT:
			exit(0);
			break;
		case SDL_KEYDOWN:
			doKeyDown(&event.key);
			break;

		case SDL_KEYUP:
			doKeyUp(&event.key);
			break;
		default:
			break;
		}
	}
}

void Input::doKeyDown(SDL_KeyboardEvent* event)
{
	if (event->repeat == 0)
	{
		if (event->keysym.scancode == SDL_SCANCODE_LEFT)
		{
			app->left = 1;
		}

		if (event->keysym.scancode == SDL_SCANCODE_RIGHT)
		{
			app->right = 1;
		}
		if (event->keysym.scancode == SDL_SCANCODE_SPACE)
		{
			app->space = 1; 
		}
	}
}

void Input::doKeyUp(SDL_KeyboardEvent* event)
{
	if (event->repeat == 0)
	{
		if (event->keysym.scancode == SDL_SCANCODE_LEFT)
		{
			app->left = 0;
		}

		if (event->keysym.scancode == SDL_SCANCODE_RIGHT)
		{
			app->right = 0;
		}
		if (event->keysym.scancode == SDL_SCANCODE_SPACE)
		{
			app->space = 0;
		}
	}
}

