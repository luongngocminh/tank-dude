#include "draw.h"
#include <SDL.h>
#include "player.h"
#include "defs.h"
#include "bullet.h"
#include <SDL_image.h>
#include <stdio.h>
#include <SDL_ttf.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
Draw::Draw(App* app) {
	this->app = app;
}

Draw::~Draw() {

}

void Draw::prepareScene() {
	SDL_RenderClear(app->renderer);
}

void Draw::presentScene() {
	SDL_RenderPresent(app->renderer);
}

void Draw::drawScore(App app, SDL_Renderer* renderer, bool checkWin) {
	TTF_Font* Arial = TTF_OpenFont("arial.ttf", 80); 
	SDL_Color Color;
	char sScore[50]="\0";
	_itoa_s(app.score, sScore, 10);
	SDL_Rect Message_rect;
	SDL_Surface* surfaceMessage;
	if (!checkWin) {
		Color = { 255, 255, 255 };
		Message_rect.x = SCREEN_WIDTH / 2 - 20;
		Message_rect.y = 10;
		Message_rect.w = 40;
		Message_rect.h = 50;
		surfaceMessage = TTF_RenderText_Solid(Arial, &sScore[0], Color);
	}
	else {
		char text[63] = "Your score: ";
		strcat_s(text, sScore);
		Color = { 255, 0, 0 };
		Message_rect.x = SCREEN_WIDTH / 2 - 200;
		Message_rect.y = SCREEN_HEIGHT / 2 - 80;
		Message_rect.w = 400;
		Message_rect.h = 120;
		surfaceMessage = TTF_RenderText_Solid(Arial, &text[0], Color);
	}
	SDL_Texture* Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);
	SDL_RenderCopy(renderer, Message, NULL, &Message_rect);
}

void Draw::drawPlayer(Player player, SDL_Renderer* renderer) {
	SDL_Rect rect;
	rect.x = player.x;
	rect.y = player.y;
	rect.w = player.w;
	rect.h = player.h;
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	SDL_RenderFillRect(renderer, &rect);
}

void Draw::drawBullet(Bullet bullet, SDL_Renderer* renderer) {
	if (bullet.appear == 1) {
		SDL_Rect rect;
		rect.x = bullet.x;
		rect.y = bullet.y;
		rect.w = BULLET_WIDTH;
		rect.h = BULLET_HEIGHT;
		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
		SDL_RenderFillRect(renderer, &rect);
	}
}

void Draw::drawTargets(Target* target, SDL_Renderer* renderer) {
	if (target->appear) {
		SDL_Surface* image = nullptr;
		switch (target->type)
		{
			case 1: //rectangle
				SDL_Rect rect;
				rect.x = target->x;
				rect.y = target->y;
				rect.w = TARGET_DIMENSION;
				rect.h = TARGET_DIMENSION;
				SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
				SDL_RenderFillRect(renderer, &rect);
				break;
			case 2: //circle
				image = IMG_Load("circle.png");
				break;
			case 3: //triangle
				image = IMG_Load("triangle.png");
				break;
			default:
				break;
		}
		SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, image);
		SDL_Rect dstrect = { target->x, target->y, TARGET_DIMENSION, TARGET_DIMENSION };
		SDL_RenderCopy(renderer, texture, NULL, &dstrect);		
	}
}

void Draw::drawTimerBar(int time_left, SDL_Renderer* renderer) {
	SDL_Rect rect;
	rect.x = 0;
	rect.y = 0;
	float temp = ((float)SCREEN_WIDTH/100) * (float)time_left / ((float)TIME_PLAY/100);
	rect.w = (int)roundf(temp * 1) / 1;
	rect.h = 10;
	SDL_SetRenderDrawColor(renderer, 255, 0,0,0);
	SDL_RenderFillRect(renderer, &rect);
}

