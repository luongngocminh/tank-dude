#include <SDL.h>
#include "player.h"
#include <stdio.h>
#include "defs.h"
Player::Player(App* app, int x, int y, int w, int h, float speed) {
	this->app = app;
	this->x = x;
	this->y = y;
	this->w = w;
	this->h = h;
	this->speed = speed;
}

Player::~Player() {

}

void Player::update() {
	if (app->left)
	{
		x -= speed;
		if (x < 0) { x = 0; }
	}

	if (app->right)
	{
		x += speed; 
		if (x + w > SCREEN_WIDTH) { x = SCREEN_WIDTH - w; }
	}
}