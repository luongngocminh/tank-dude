#ifndef STRUCTS_H
#define STRUCTS_H
#include <SDL.h>

typedef struct {
	SDL_Renderer* renderer;
	SDL_Window* window;
	int left;
	int right;
	int space;
	int score;
}App;
#endif // !STRUCTS_H