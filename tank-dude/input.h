#ifndef INPUT_H
#define INPUT_H
#include "structs.h"
class Input
{
public:
	Input(App* app);
	~Input();

	void doInput();
	void doNewFrame();
	void doKeyDown(SDL_KeyboardEvent* event);
	void doKeyUp(SDL_KeyboardEvent* event);
private:
	App* app;
};
#endif // !INPUT_H


