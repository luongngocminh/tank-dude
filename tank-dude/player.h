#ifndef PLAYER_H
#define PLAYER_H
#include "structs.h"

class Player {
public:
	Player(App* app, int x, int y, int w, int h, float speed);
	~Player();
	int x;
	int y;
	int w;
	int h;
	float speed;
	void update();
private:
	App* app;
};
#endif // !PLAYER_H
